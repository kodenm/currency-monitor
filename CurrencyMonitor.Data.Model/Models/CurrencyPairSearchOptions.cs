﻿namespace CurrencyMonitor.Data.Model.Models;

/// <summary>
/// Describes which base currencies converted to quote currency should be taken for specified period of time
/// </summary>
public class CurrencyPairSearchOptions
{
    /// <summary>
    /// List of base currenciy's codes that should be taken
    /// </summary>
    public List<string> BaseCurrencyCodes { get; set; }

    /// <summary>
    /// Quote currency's code
    /// </summary>
    public string QuoteCurrencyCode { get; set; }

    /// <summary>
    /// Start Date
    /// </summary>
    public DateOnly DateFrom { get; set; }

    /// <summary>
    /// End Date
    /// </summary>
    public DateOnly DateTo { get; set; }
}