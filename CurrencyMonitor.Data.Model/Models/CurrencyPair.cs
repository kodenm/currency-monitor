﻿namespace CurrencyMonitor.Data.Model.Models;

/// <summary>
/// Quotation of two different currencies, with the value of one currency being quoted against the other
/// </summary>
public class CurrencyPair
{
    /// <summary>
    /// Value to be converted from
    /// </summary>
    public Currency BaseCurrency { get; init; }

    /// <summary>
    /// Value to be converted to
    /// </summary>
    public Currency QuoteCurrency { get; init; }

    /// <summary>
    /// Date which quote is valid for
    /// </summary>
    public DateOnly Date { get; init; }

    /// <summary>
    /// Shows how many <see cref="QuoteCurrency"/> can be exchanged for one <see cref="BaseCurrency"/>
    /// </summary>
    public decimal Quote { get; init; }
}
