﻿namespace CurrencyMonitor.Data.Model.Models;

/// <summary>
/// Contains information about one currency
/// </summary>
public class Currency
{
    /// <summary>
    /// Local currency's code
    /// </summary>
    public string CurrencyCode { get; init; }

    /// <summary>
    /// Local currency's name
    /// </summary>
    public string Name { get; init; }

    /// <summary>
    /// International currency's name
    /// </summary>
    public string EnglishName { get; init; }

    /// <summary>
    /// ISO currency's numerical code
    /// </summary>
    public int ISONumCode { get; init; }

    /// <summary>
    /// ISO currency's character code
    /// </summary>
    public string ISOCharCode { get; init; }
}
