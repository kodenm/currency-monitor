﻿using CurrencyMonitor.Data.Model.Models;

namespace CurrencyMonitor.Data.Model.Interfaces;

/// <summary>
/// Contains basic methods for working with currencies
/// </summary>
public interface ICurrencyService
{
    /// <summary>
    /// Lists all known currencies
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<Currency>> GetCurrenciesAsync();

    /// <summary>
    /// Lists all currency pair records for specified period of time
    /// </summary>
    /// <param name="searchOptions">Describes which base currencies converted to quote currency should be taken for specified period of time</param>
    /// <returns></returns>
    Task<IEnumerable<CurrencyPair>> GetCurrencyPairsAsync(CurrencyPairSearchOptions searchOptions);
}
