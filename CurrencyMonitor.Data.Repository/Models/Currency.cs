﻿namespace CurrencyMonitor.Data.Repository.Models;

public class Currency
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string EnglishName { get; set; }
    public int ISONumCode { get; set; }
    public string ISOCharCode { get; set; }
    public List<CurrencyPair> CurrencyPairsAsBase { get; set; }
    public List<CurrencyPair> CurrencyPairsAsQuote { get; set; }
}