﻿namespace CurrencyMonitor.Data.Repository.Models;

public class CurrencyPair
{
    public int Id { get; set; }
    public string BaseCurrencyId { get; set; }
    public Currency BaseCurrency { get; set; }
    public string QuoteCurrencyId { get; set; }
    public Currency QuoteCurrency { get; set; }
    public DateOnly Date { get; set; }
    public decimal Quote { get; set; }
}