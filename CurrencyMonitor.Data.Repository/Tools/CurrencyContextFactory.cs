﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace CurrencyMonitor.Data.Repository.Tools;

public class CurrencyContextFactory : IDesignTimeDbContextFactory<CurrencyContext>
{
    public CurrencyContext CreateDbContext(string[] args)
    {
        var config = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        var connectionString = config.GetConnectionString("postgres");
        var options = new DbContextOptionsBuilder()
            .UseNpgsql(connectionString)
            .UseSnakeCaseNamingConvention()
            .Options;
        return new CurrencyContext(options);
    }
}
