﻿using AutoMapper;
using CurrencyMonitor.Data.Model.Interfaces;
using CurrencyMonitor.Data.Model.Models;
using Models = CurrencyMonitor.Data.Model.Models;
using EF = CurrencyMonitor.Data.Repository.Models;

namespace CurrencyMonitor.Data.Repository.Services;

public class CurrencyService : ICurrencyService
{
    private CurrencyContext _currencyContext;
    private IMapper _mapper;

    public CurrencyService(CurrencyContext currencyContext, IMapper mapper)
    {
        _currencyContext = currencyContext;
        _mapper = mapper;
    }

    public Task<IEnumerable<Currency>> GetCurrenciesAsync()
    {
        return new Task<IEnumerable<Currency>>(() => _mapper.Map<IEnumerable<Currency>>(_currencyContext.Currencies.ToList()));
    }

    public Task<IEnumerable<CurrencyPair>> GetCurrencyPairsAsync(CurrencyPairSearchOptions searchOptions)
    {
        var result = _currencyContext.CurrencyPairs.Where(
            cp => searchOptions.BaseCurrencyCodes.Contains(cp.BaseCurrencyId)
            && searchOptions.QuoteCurrencyCode.Equals(cp.QuoteCurrencyId)
            && searchOptions.DateFrom >= cp.Date && searchOptions.DateTo <= cp.Date);
        return new Task<IEnumerable<CurrencyPair>>(() => _mapper.Map<IEnumerable<CurrencyPair>>(result));
    }
}