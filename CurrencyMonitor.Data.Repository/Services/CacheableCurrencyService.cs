﻿using AutoMapper;
using CurrencyMonitor.Data.Model.Interfaces;
using CurrencyMonitor.Data.Model.Models;

namespace CurrencyMonitor.Data.Repository.Services;

// NOTE: MOVE THIS OUT TO A DIFFERENT PROJECT. THIS SHOULDN'T BE HERE 'CAUSE THE PROJECT IS RELATED TO DATABASE LAYER
public class CacheableCurrencyService : ICurrencyService
{
    private CurrencyContext _currencyContext;
    private ICurrencyService _innerCurrencyService;
    private IMapper _mapper;

    public CacheableCurrencyService(ICurrencyService innerCurrencyService, CurrencyContext currencyContext, IMapper mapper)
    {
        _innerCurrencyService = innerCurrencyService;
        _currencyContext = currencyContext;
        _mapper = mapper;
    }

    public Task<IEnumerable<Currency>> GetCurrenciesAsync()
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<CurrencyPair>> GetCurrencyPairsAsync(CurrencyPairSearchOptions searchOptions)
    {
        throw new NotImplementedException();
    }
}