﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace CurrencyMonitor.Data.Repository.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "currency",
                columns: table => new
                {
                    id = table.Column<string>(type: "text", nullable: false),
                    name = table.Column<string>(type: "text", nullable: false),
                    english_name = table.Column<string>(type: "text", nullable: false),
                    iso_num_code = table.Column<int>(type: "integer", nullable: false),
                    iso_char_code = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_currency", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "currency_pair",
                columns: table => new
                {
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    base_currency_id = table.Column<string>(type: "text", nullable: true),
                    quote_currency_id = table.Column<string>(type: "text", nullable: true),
                    date = table.Column<DateOnly>(type: "date", nullable: false),
                    quote = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_currency_pair", x => x.id);
                    table.ForeignKey(
                        name: "fk_currency_pair_currency_base_currency_id",
                        column: x => x.base_currency_id,
                        principalTable: "currency",
                        principalColumn: "id");
                    table.ForeignKey(
                        name: "fk_currency_pair_currency_quote_currency_id",
                        column: x => x.quote_currency_id,
                        principalTable: "currency",
                        principalColumn: "id");
                });

            migrationBuilder.CreateIndex(
                name: "ix_currency_pair_base_currency_id",
                table: "currency_pair",
                column: "base_currency_id");

            migrationBuilder.CreateIndex(
                name: "ix_currency_pair_quote_currency_id",
                table: "currency_pair",
                column: "quote_currency_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "currency_pair");

            migrationBuilder.DropTable(
                name: "currency");
        }
    }
}
