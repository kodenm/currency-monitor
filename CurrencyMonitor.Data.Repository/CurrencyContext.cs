﻿using CurrencyMonitor.Data.Repository.Configurations;
using CurrencyMonitor.Data.Repository.Models;
using Microsoft.EntityFrameworkCore;

namespace CurrencyMonitor.Data.Repository;

public class CurrencyContext : DbContext
{
    public DbSet<Currency> Currencies { get; set; }
    public DbSet<CurrencyPair> CurrencyPairs { get; set; }

    public CurrencyContext(DbContextOptions options) : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .ApplyConfiguration(new CurrencyConfiguration())
            .ApplyConfiguration(new CurrencyPairConfiguration());
    }
}