﻿using AutoMapper;
using EFCurrencyPair = CurrencyMonitor.Data.Repository.Models.CurrencyPair;
using ModelCurrencyPair = CurrencyMonitor.Data.Model.Models.CurrencyPair;

namespace CurrencyMonitor.Data.Repository.MapperProfiles;

public class CurrencyPairProfile : Profile
{
    public CurrencyPairProfile()
    {
        CreateMap<EFCurrencyPair, ModelCurrencyPair>()
            .ForMember(d => d.BaseCurrency, opts => opts.MapFrom(s => s.BaseCurrency))
            .ForMember(d => d.QuoteCurrency, opts => opts.MapFrom(s => s.QuoteCurrency))
            .ForMember(d => d.Date, opts => opts.MapFrom(s => s.Date))
            .ForMember(d => d.Quote, opts => opts.MapFrom(s => s.Quote));
    }
}