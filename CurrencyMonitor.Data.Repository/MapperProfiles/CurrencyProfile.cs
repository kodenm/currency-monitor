﻿using AutoMapper;
using EFCurrency = CurrencyMonitor.Data.Repository.Models.Currency;
using ModelCurrency = CurrencyMonitor.Data.Model.Models.Currency;

namespace CurrencyMonitor.Data.Repository.MapperProfiles;

public class CurrencyProfile : Profile
{
    public CurrencyProfile()
    {
        CreateMap<EFCurrency, ModelCurrency>()
            .ForMember(d => d.CurrencyCode, opts => opts.MapFrom(s => s.Id))
            .ForMember(d => d.Name, opts => opts.MapFrom(s => s.Name))
            .ForMember(d => d.EnglishName, opts => opts.MapFrom(s => s.EnglishName))
            .ForMember(d => d.ISOCharCode, opts => opts.MapFrom(s => s.ISOCharCode))
            .ForMember(d => d.ISONumCode, opts => opts.MapFrom(s => s.ISONumCode));
    }
}