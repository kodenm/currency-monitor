﻿using CurrencyMonitor.Data.Repository.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CurrencyMonitor.Data.Repository.Configurations;

public class CurrencyPairConfiguration : IEntityTypeConfiguration<CurrencyPair>
{
    public void Configure(EntityTypeBuilder<CurrencyPair> builder)
    {
        builder.ToTable("currency_pair");

        builder.HasKey(cp => cp.Id);
        builder
            .HasOne(cp => cp.BaseCurrency).WithMany(c => c.CurrencyPairsAsBase)
            .HasForeignKey(cp => cp.BaseCurrencyId);
        builder
            .HasOne(cp => cp.QuoteCurrency).WithMany(c => c.CurrencyPairsAsQuote)
            .HasForeignKey(cp => cp.QuoteCurrencyId);
        builder.Property(cp => cp.Date).IsRequired();
        builder.Property(cp => cp.Quote).IsRequired();
    }
}