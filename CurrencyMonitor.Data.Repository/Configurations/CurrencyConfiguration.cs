﻿using CurrencyMonitor.Data.Repository.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CurrencyMonitor.Data.Repository.Configurations;

public class CurrencyConfiguration : IEntityTypeConfiguration<Currency>
{
    public void Configure(EntityTypeBuilder<Currency> builder)
    {
        builder.ToTable("currency");

        builder.HasKey(c => c.Id);
        builder.Property(c => c.Name).IsRequired();
        builder.Property(c => c.EnglishName).IsRequired();
        builder.Property(c => c.ISONumCode).IsRequired();
        builder.Property(c => c.ISOCharCode).IsRequired();
    }
}